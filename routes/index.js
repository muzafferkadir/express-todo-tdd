var express = require('express');
var router = express.Router();
const Todo = require('../models/todo');

router.post('/', async function (req, res) {
  if (!req.body.todo) {
    res.status(400).send()
    return
  }

  try {
    const potentialTodo = await Todo.create({ text: req.body.todo })

    res.status(201).send({ todo: potentialTodo.text });

  } catch (error) {
    console.log(error);
    res.status(500).send(error)
  }
});

module.exports = router;
