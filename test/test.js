const request = require('supertest');
const app = require('../app');

describe("POST a Todo", () => {
  let data = {
    todo: "Buy Tomato"
  };

  it("Creates a Todo", done => {
    request(app)
      .post("/")
      .send(data)
      .expect({
        todo: "Buy Tomato"
      })
      .expect(201, done);
  });
});